import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
declare var $;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  @ViewChild('dataTable') table:ElementRef;
  dataTable:any;
  constructor(){}
  ngOnInit():void {
    this.dataTable=$(this.table.nativeElement);
    this.dataTable.dataTable();
  }
}
